# ECOMOD Publications

This repository stores the ecomod group publications database.
This readme summarizes how to add new publications to the database which will then be pushed automatically to the homepage via bibbase.org.


## Part 1: Technical setup with GCMS

On our ecomod GCMS homepage, we want to present our publications. Previously this was done with simple text, but that is tedious to update. The current solution is based on a .bib file and the bibbase.org service. It builds a database on the homepage automatically via java script. It also allows for customizing these tables with additional flags (sorting, grouping, etc.). The tables can additionally be styled via css (removing certain links, fields, buttons, ...).

### Hosting of the bib file

Previously, we used the GCMS Document database to host the .bib file. However, since the update 2020, we cannot upload .bib files, and even worse, each time the file is updated the link to the file changes, which means we have to update the links on our pages everytime we upload a new version. Instead, the .bib file is now hosted here in this github repository within the ecomod gitlab group.

### Hosting of the PDF files

The PDF files are hosted on the GCMS. The PDFs can be uploaded via the GCMS admin system. The file links need to be added to the .bib database (see guide section). The PDF links are only visible on the hidden page, which need a username and a password.

### Adding database tables to the GCMS pages

Publication databases are added at four locations:

* The main welcome page, where we only show recent papers (currently 2018/2019/2020)
* The publications page (here we show all publications, but no pdf links)
* The hidden publications page (including pdf links, can only be accessed with username and password)
* Personal staff pages (e.g. Jan Salecker, Claudia Dislich)

All these pages draw the data from the same bib file, but are formatted differently by tag filtering and css files.

#### Welcome page

`<script src="//bibbase.org/show?bib=https://gitlab.gwdg.de/ecomod/ecomod_publications/-/raw/master/bib/ecomod_publications.bib&jsonp=1&sort=author_short&filter=comment:frontpage"></script>`

`<link href="https://www.uni-goettingen.de/docs/07d33cd765e5221e6f53a08b8fa8689d.css" rel="stylesheet">`

The database on the welcome page filters the comment field for the tag "frontpage".

Thus, if you want to show an article on the frontpage, you need to add the keyword "frontpage" into the comment field of an article in the bib database.

The css file removes pdf links and download buttons.

#### Main publication page

`<script src="//bibbase.org/show?bib=https://gitlab.gwdg.de/ecomod/ecomod_publications/-/raw/master/bib/ecomod_publications.bib&jsonp=1&sort=author_short&filter=comment:public"></script>`

`<link href="https://www.uni-goettingen.de/docs/07d33cd765e5221e6f53a08b8fa8689d.css" rel="stylesheet">`

The database on the main publications page filters the comment field for the tag "public".

Thus, only articles with the keyword public are shown. If you want to add articles to the bib database that should not be shown on the homepage, just remove the public keyword from the comments field.

The css file removes pdf links and download buttons.

#### Hidden publications page

`<script src="//bibbase.org/show?bib=https://gitlab.gwdg.de/ecomod/ecomod_publications/-/raw/master/bib/ecomod_publications.bib&jsonp=1&sort=author_short"></script>`

Here all articles from the bib file are shown (no filtering). Also, there is no additional css style sheet meaning, the pdf link and download buttons are visible.

#### Personal staff pages

`<script src="//bibbase.org/show?bib=https://gitlab.gwdg.de/ecomod/ecomod_publications/-/raw/master/bib/ecomod_publications.bib&jsonp=1&filter=authors:Salecker"></script>`

`<link href="https://www.uni-goettingen.de/docs/07d33cd765e5221e6f53a08b8fa8689d.css" rel="stylesheet">`

Here entries are filtered by author (here "Salecker"). The css style sheet removes pdf links.



### Access to hidden page

https://www.uni-goettingen.de/de/579342.html 

Username: Ecomod 
Password: ecosystem-modelling 



## Part 2: The bib file

The bib file can be opened and edited with https://www.jabref.org/

In most parts, it is a usual bibtex database. However, three details need further mentioning:

1. Highlight colors -> articles that are currently displayed on the publication board are highlighted in yellow

2. Comment field -> The comment field of articles is used for filtering on the homepage. If you want an article to be shown you need to add "public" in the comment field. If you want it to be shown on the frontpage you need to add "frontpage" in the comment field. If you want both, you need to add "public frontpage" (thats what we usually do for new articles).

3. Optional fields "Url_pdf" -> this is the field where we add the links to the PDF files that are hosted in the GCMS (e.g. https://uni-goettingen.de/de/document/download/5d01e2e6fbc6329cb39bb248dd8dd885.pdf/Salecker_et_al_2019_MEE_Chapter2.pdf).

   This field (url_pdf) might not be visible for you, depending on your jabref configuration.

   In order to see the url_pdf field in Jabref go to "Bibtex" -> "Customize Entry Types"
   For the document types you wish, add the field "url_pdf" to the optional fields section.



## Part 3: A workflow example


First you need to set things up (have a connection to this repo on your local PC).
You can either create a clone using your preferred git managing software (e.g. gitkraken) or you can also use RStudios git tab, which I find quite convenient. Just open Rstudio click on File -> New Project -> Version Control -> Git

Then copy the clone link from this repository and select a folder location on your harddrive.
Now you have a copy of this repo on your PC. You can now edit the bib file and push changes via the git tab, driectly in RStudio.

Lets assume there is a new publication in our group. Here is the exact workflow:

0. Make sure you have the recent version of the repo -> execute git PULL

1. Create a new article in the "ecomod_publications.bib" file

   a) in most cases a bibtex file can be downloaded from the article page. Just open it, and copy paste it into the database.

   b) Sometimes articles need to be added manually

2. Upload the pdf of the article to the GCMS

   Go to the document browser of the GCMS -> Navigate into the folder "publications"

   In the upeer right click "Operationen" -> "Neue Dokumente hochladen"

   Choose the pdf (needs to be <10mb, resave with pdf24 to make large files smaller) and upload it

   Save with OK

   Click on the uploaded file icon in the GCMS to open the file popup

   Right-click on the blue link of the file in the pop-up window and click "copy link address"

3. Edit bibtex entry

   Edit the currently added article in jabref

   Paste the copied pdf link (GCMS) into the "Url_pdf" field in the "Other fields" tab

   Add "public frontpage" to the comment field if the article should be public and shown on the frontpage

4. Save, commit and Push changes

   Save the bib file and push the updated file to the repository via git commit and git push

5. Publication board

   Remove the highlight of the oldest highlighted article and highlight the recently added article in jabref

   Save the database

   Print out the first page of the article

   Take the oldest article board from the publication board (bottom board on the right side of the pillar)

   Move all boards one step forward

   Put article frontpage in the removed board and put it on at the front row!